import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-simple-form',
  templateUrl: './simple-form.component.html',
  styleUrls: ['./simple-form.component.css']
})
export class SimpleFormComponent implements OnInit {

  public name: string = 'World!';

  constructor() { }

  ngOnInit(): void {
    // this.name = 'Wereld';
  }

  onClick() {
    this.name = 'Foo';
  }


}

import {Component, EventEmitter, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {filter, map, reduce, tap} from 'rxjs/operators';
import {from, Observable, Subscription} from 'rxjs';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit, OnDestroy {

  public userDetails: FormGroup;

  public myObservable : Subject<string> = new Subject();

  private subscriptions: Subscription[] = [];

  constructor(formBuilder: FormBuilder) {
    this.userDetails = formBuilder.group({
      email: ['', {validators: [Validators.email, Validators.min(3), Validators.required], updateOn: 'change'}],
      firstName: ['', {updateOn: 'change'}],
      lastName: ['Last Name']
    });
  }

  ngOnInit(): void {
    const valueChangesSub = this.userDetails.valueChanges
      .pipe(
        // filter(values => values.firstName),
        tap(console.log),
        tap(() => this.userDetails.controls['email']?.setValue('foo@mail.com')),
      )
      .subscribe();
    this.subscriptions.push(valueChangesSub);
    this.userDetails.controls?.firstName?.valueChanges.subscribe(
      (values) => {
        console.log('I am in the subscription', values)
      }
    )
  }

  public onSubmit() {
    console.log(this.userDetails.value)
    this.myObservable.next("User Details have been submitted")
  }

  public controlHasError(formControlName: string) {
    return this.userDetails.get(formControlName)?.dirty && this.userDetails.get(formControlName)?.errors
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
